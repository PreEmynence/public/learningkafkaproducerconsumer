package com.preemynence.kafkaLearning.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.preemynence.kafkaLearning.model.Model;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class ModelSerializer implements Serializer<Model>
{
    @Override
    public void configure(Map<String, ?> map, boolean b)
    {
    }

    @Override
    public byte[] serialize(String arg0, Model arg1)
    {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            retVal = objectMapper.writeValueAsString(arg1).getBytes();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return retVal;
    }

    @Override
    public void close()
    {
    }
}