package com.preemynence.kafkaLearning.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.preemynence.kafkaLearning.model.Model;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class ModelDeserializer
    implements Deserializer<Model>
{
    @Override
    public void close()
    {
    }

    @Override
    public void configure(Map<String, ?> arg0, boolean arg1)
    {
    }

    @Override
    public Model deserialize(String arg0, byte[] arg1)
    {
        ObjectMapper mapper = new ObjectMapper();
        Model model = null;
        try
        {
            model = mapper.readValue(arg1, Model.class);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return model;
    }
}