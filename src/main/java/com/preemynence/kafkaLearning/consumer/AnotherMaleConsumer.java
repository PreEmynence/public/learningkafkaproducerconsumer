package com.preemynence.kafkaLearning.consumer;

import com.preemynence.kafkaLearning.model.Gender;
import com.preemynence.kafkaLearning.model.Model;
import com.preemynence.kafkaLearning.serializer.ModelDeserializer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collections;
import java.util.Properties;

public class AnotherMaleConsumer
{
    private final static String TOPIC             = "twoPartitionTopic";
    private final static String BOOTSTRAP_SERVERS = "http://localhost:9092";

    private static Consumer<Gender, Model> createConsumer()
    {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "maleCollector");
        //        props.put(ConsumerConfig.FETCH_MAX_BYTES_CONFIG, 10);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 2);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ModelDeserializer.class.getName());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");

        // Create the consumer using props.
        final Consumer<Gender, Model> consumer = new KafkaConsumer<>(props);
        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }

    public static void main(String... args)
    {
        runConsumer();
    }

    static void runConsumer()
    {
        final Consumer<Gender, Model> consumer = createConsumer();
        final int giveUp = 600; //60 seconds run time
        int noRecordsCount = 0;
        while (true)
        {
            final ConsumerRecords<Gender, Model> consumerRecords = consumer.poll(1000);

            int count = consumerRecords.count();

            if (count == 0)
            {
                noRecordsCount++;
                if (noRecordsCount > giveUp) { break; }
                else
                {
                    System.out.print(".");
                    continue;
                }
            }

            consumerRecords.forEach(record -> {
                if (Gender.MALE.equals(record.value().getGender()))
                {
                    System.out.printf("\nConsumer Record:(%s, %s, %d, %d)",
                                      record.key(), record.value(),
                                      record.partition(), record.offset());
                }
            });
            consumer.commitSync();
        }
        consumer.commitSync();
        consumer.close();
        System.out.println("DONE");
    }
}
