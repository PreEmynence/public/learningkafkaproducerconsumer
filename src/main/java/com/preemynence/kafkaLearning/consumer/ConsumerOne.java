package com.preemynence.kafkaLearning.consumer;

import com.preemynence.kafkaLearning.model.Gender;
import com.preemynence.kafkaLearning.model.Model;
import com.preemynence.kafkaLearning.serializer.ModelDeserializer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import static java.lang.Thread.sleep;

public class ConsumerOne
{
    private final static String TOPIC             = "two";
    private final static String BOOTSTRAP_SERVERS = "http://localhost:9092";

    private static Consumer<String, Model> createConsumer()
    {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "Collector");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 10);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ModelDeserializer.class.getName());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        // Create the consumer using props.
        final Consumer<String, Model> consumer = new KafkaConsumer<>(props);
        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }

    public static void main(String... args)
    {
        runConsumer();
    }

    static void runConsumer()
    {
        final Consumer<String, Model> consumer = createConsumer();
        final int giveUp = 600;
        int noRecordsCount = 0;
        Map<TopicPartition, OffsetAndMetadata> offsets = new HashMap<>();
        while (true)
        {
            if (!offsets.keySet().isEmpty())
            {
                for (TopicPartition topicPartition : consumer.assignment())
                {
                    if (offsets.containsKey(topicPartition))
                    {
                        consumer.seek(topicPartition, offsets.get(topicPartition));
                    }
                }
            }
            final ConsumerRecords<String, Model> records = consumer.poll(1000);

            int consumeRecords = (records.count() > 0) ? new Random().nextInt(records.count()) : 0;

            if (records.count() <= 1)
            {
                noRecordsCount++;
                if (noRecordsCount > giveUp) { break; }
                else
                {
                    System.out.print(".");
                    continue;
                }
            }

            int count = 0;
            for (ConsumerRecord<String, Model> record : records)
            {
                count++;
                if (count > consumeRecords)
                {
                    TopicPartition topicPartition = new TopicPartition(record.topic(), record.partition());
                    OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(record.offset());
                    offsets.put(topicPartition, offsetAndMetadata);
                    break;
                }

                System.out.println(
                    "Consumed (" + (count - 1) + ", " + consumeRecords + ") record of partition " + record.partition()
                    + ", at " + +record.offset() + " polled " + records.count());
            }
            consumer.commitSync(offsets);
        }
        consumer.commitSync();
        consumer.close();
        System.out.println("DONE");
    }
}