package com.preemynence.kafkaLearning.consumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collections;
import java.util.Properties;

public class ThreadedConsumer
{
    private final static String TOPIC             = "test";
    private final static String BOOTSTRAP_SERVERS = "http://localhost:9092";

    private static Consumer<Long, String> createConsumer()
    {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "myGroup");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");

        // Create the consumer using props.
        final Consumer<Long, String> consumer = new KafkaConsumer<>(props);
        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }

    public static void main(String... args)
    {
        runConsumer();
    }

    static void runConsumer()
    {
        final Consumer<Long, String> consumer = createConsumer();
        final int giveUp = 200;
        int noRecordsCount = 100;
        while (true) {
            final ConsumerRecords<Long, String> consumerRecords =
                consumer.poll(1000);
            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }
            consumerRecords.forEach(record -> {
                System.out.printf("Consumer Record:(%d, %s, %d, %d)\n",
                                  record.key(), record.value(),
                                  record.partition(), record.offset());
            });
            consumer.commitSync();
        }
        consumer.commitSync();
        consumer.close();
        System.out.println("DONE");
    }
}
