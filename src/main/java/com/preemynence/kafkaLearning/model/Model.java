package com.preemynence.kafkaLearning.model;

public class Model
{
    private String name;
    private long    age;
    private Gender gender;

    public Model()
    {
    }

    public Model(String name, long age, Gender gender)
    {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName()
    {
        return this.name;
    }

    public long getAge()
    {
        return this.age;
    }

    public Gender getGender()
    {
        return gender;
    }

    @Override
    public String toString()
    {
        return "Model(" + name + ", " + age + ", " + gender + ")";
    }
}