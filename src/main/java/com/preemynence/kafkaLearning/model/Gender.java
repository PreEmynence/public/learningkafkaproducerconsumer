package com.preemynence.kafkaLearning.model;

public enum Gender
{
    MALE,
    FEMALE
}