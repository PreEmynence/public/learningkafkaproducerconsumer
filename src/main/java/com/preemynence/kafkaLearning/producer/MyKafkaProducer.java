package com.preemynence.kafkaLearning.producer;

import com.preemynence.kafkaLearning.model.Gender;
import com.preemynence.kafkaLearning.model.Model;
import com.preemynence.kafkaLearning.partitioner.ModelPartitioner;
import com.preemynence.kafkaLearning.serializer.ModelSerializer;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MyKafkaProducer
{
    private final static String TOPIC             = "two";
    private final static String BOOTSTRAP_SERVERS = "http://localhost:9092";

    public static void main(String... args)
        throws Exception
    {
        if (args.length == 0)
        {
            runProducer(200);
        }
        else
        {
            runProducer(Integer.parseInt(args[0]));
        }
    }

    private static Producer<String, Model> createProducer()
    {
        Properties props = new Properties();
        props.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(CommonClientConfigs.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ModelSerializer.class.getName());
        props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, ModelPartitioner.class.getCanonicalName());
        props.put("partition.0", "FEMALE");
        props.put("partition.1", "MALE");
        return new KafkaProducer<>(props);
    }

    static void runProducer(final int sendMessageCount)
        throws InterruptedException
    {
        final Producer<String, Model> producer = createProducer();
        long time = System.currentTimeMillis();
        final CountDownLatch countDownLatch = new CountDownLatch(sendMessageCount);

        try
        {
            for (long index = time; index < time + sendMessageCount; index++)
            {
                int i = new Random().nextInt(50);
                Gender gender = (i % 2 == 0 ? Gender.MALE : Gender.FEMALE);
                Model model = new Model("Vishal - ", i, gender);
                final ProducerRecord<String, Model> record = new ProducerRecord<>(TOPIC, model.getGender().name(), model);
                producer.send(record, (metadata, exception) -> {
                    long elapsedTime = System.currentTimeMillis() - time;
                    if (metadata != null)
                    {
                        System.out.printf("sent record(key=%s value=%s) " +
                                          "meta(partition=%d, offset=%d) time=%d\n",
                                          record.key(), record.value(), metadata.partition(),
                                          metadata.offset(), elapsedTime);
                    }
                    else
                    {
                        exception.printStackTrace();
                    }
                    countDownLatch.countDown();
                });
            }
            countDownLatch.await(25, TimeUnit.SECONDS);
        }
        finally
        {
            producer.flush();
            producer.close();
        }
    }

}