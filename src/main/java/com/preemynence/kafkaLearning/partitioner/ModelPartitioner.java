package com.preemynence.kafkaLearning.partitioner;

import com.preemynence.kafkaLearning.model.Model;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.HashMap;
import java.util.Map;

public class ModelPartitioner
    implements Partitioner
{
    private static Map<String, Integer> modelToPartitionMap;

    @Override
    public void configure(Map<String, ?> configs)
    {
        System.out.println("Inside ModelPartitioner.configure " + configs);
        modelToPartitionMap = new HashMap<>();
        for (Map.Entry<String, ?> entry : configs.entrySet())
        {
            if (entry.getKey().startsWith("partition."))
            {
                String keyName = entry.getKey();
                String value = (String) entry.getValue();
                System.out.println(keyName.substring(11));
                int partitionId = Integer.parseInt(keyName.substring(10));
                modelToPartitionMap.put(value, partitionId);
            }
        }
    }

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes,
                         Cluster cluster)
    {
        Model valueStr = (Model) value;
        String gender = valueStr.getGender().name();
        if (modelToPartitionMap.containsKey(gender))
        {
            return modelToPartitionMap.get(gender);
        }
        else
        {
            int noOfPartitions = cluster.topics().size();
            return value.hashCode() % noOfPartitions + modelToPartitionMap.size();
        }
    }

    public void close() {}
}