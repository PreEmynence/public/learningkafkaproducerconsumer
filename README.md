# LearningKafkaProducerConsumer

This project explores the sample dockerized implementation of the slimmest working Kafka setup. And we will run producer consumers on it. 

Steps to run and test application.
- Install Docker
- docker-compose -f docker-compose.yml up -d (docker-compose.yml is in resources)
- wait for 5-10 minutes it will download and setup single container kafka and zookeeper for your testing.
- There is a Producer (MyKafkaProducer)
- There are two different Consumers which both are listening with same group.id means if you run both of them only one will receive the message, from the last read message.

For creating the topic with multiple partition
- Run the CreateTopic class to create multiple partition topic.
- find the network created by running the example.

Added the capabilities to consume partially polled messages.
- Now if we are polling multiple messages but based on some condition if we want to postpone the rest of the messages, we can commit what ever we have consumed, and we can seek to the next offset before polling again to start consuming. 
- Run the ConsumerOne and ConsumerTwo first and then run the MyKafkaProducer.

---
Some kafka util Commands needed to explore the kafka in commandline in docker container

- docker ps 
    - this will list two docker containes with name kafkaleaning_kafka_1 and kafkaleaning_zookeeper_1
    
- docker network list
    - this will display the networks created by all the docker setup
    
- docker network inspect {kafkaleaning_default}
    - This will display all the network information of the setup.
    - In this look for the IPv4Address field of zookeeper and kafka, which will be needed if you want to create topic from commandline.
    
- ping {IP of the zookeeper from kafka docker}
    - if results -> 64 bytes from {IP}: icmp_seq=1 ttl=64 time=1.93 ms
    - you are good.
    
- nc -vz localhost 9092
    - verify that kafka is running on 9092 port of the cotainer.
    
- kafka-console-consumer --bootstrap-server kafka:29092 --topic <topic_name> --from-beginning
    - This lists all the events of topic mentioned from the beginning.
    
- kafka-consumer-groups --bootstrap-server kafka:29092 --list
    - This lists all the groups of kafka consumers.

- kafka-topics --zookeeper {ZooKeeperIP}:2181 --topic <topic> --describe
    - will results something like following
    Topic:sophosTopic	PartitionCount:1	ReplicationFactor:1	Configs:
    	Topic: sophosTopic	Partition: 0	Leader: 1	Replicas: 1	Isr: 1
    	
- kafka-topics --zookeeper {ZooKeeperIP}:2181 --create --topic twoPartitionTopic --partitions 2 --replication-factor 1
    - this will create topic with two partitions
    - kafka-topics --zookeeper {ZooKeeperIP}:2181 --topic twoPartitionTopic --describe
    Topic:twoPartitionTopic	PartitionCount:2	ReplicationFactor:1	Configs:
    	Topic: twoPartitionTopic	Partition: 0	Leader: 1	Replicas: 1	Isr: 1
    	Topic: twoPartitionTopic	Partition: 1	Leader: 1	Replicas: 1	Isr: 1 

- kafka-topics --zookeeper {ZooKeeperIP}:2181 --delete --topic twoPartitionTopic
    - this will try to delete the topic
    - with following condition
        - /etc/kafka/server.properties should have following line 
        - delete.topic.enable = true